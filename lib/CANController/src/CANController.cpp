#include "CANController.h"

// CANParameter
CANParameter::CANParameter(uint16_t id){
    this->id = id;
}

void CANParameter::parse(uint8_t nBytes, uint8_t payload[64]) {
    float tempVal = 0.0;
    int byteRes = 0; 

    if(bitLen < 8) {
        int byteMask = ((1<<bitLen)-1) << (posBit-1);;
        byteRes = (payload[posByte-1] & byteMask) >> (posBit-1);
    } else {
        int lengthBytes = int(bitLen/8);
        int pos = posByte-1;
        byteRes  = 0;
        for(int i=0; i<lengthBytes; i++) {   
            byteRes += payload[pos] << (i*8);
            pos++;
        }
    }

    tempVal = byteRes * resolution + offset;
        if(tempVal < minVal) tempVal = minVal;
        else if(tempVal > maxVal) tempVal = maxVal;

    paramVal = tempVal;
}

float CANParameter::getVal() {
    return (paramVal);
}

// CANPacket
CANPacket::CANPacket(uint32_t id) {
    this->id = id;
}

bool CANPacket::addParameter(CANParameter *paramPtr) {
    int paramId = paramPtr->id;
    if (!hasParam(paramId)) {
        paramList.insert({paramId, paramPtr});
        return (true);
    } 
    else return (false);
}

bool CANPacket::getParamVal(uint16_t paramId, float &retVal) {
    if (this->isValid()) {
        if (this->hasParam(paramId)) {
            retVal = paramList.at(paramId)->getVal();
            return (true);
        }
    }
    return (false);
}

void CANPacket::parse(uint8_t nBytes, uint8_t payload[64]) {
    lastRec = millis();

    for(auto & element : paramList) {
        element.second->parse(nBytes, payload);
    }
}

bool CANPacket::hasParam(uint16_t paramId) {
    return (paramList.find(paramId) != paramList.end());
}


// CANController
CANController::CANController() {

}

bool CANController::addPacket(CANPacket *packetPtr) {
    uint32_t id = packetPtr->id;
    if (!hasPacket(id)) {
        packetList.insert({id, packetPtr});
        return (true);
    }
    else return (false);
}

bool CANController::parse(uint32_t id, uint8_t nBytes, uint8_t payload[64]) {
    if (hasPacket(id)) {
        packetList.at(id)->parse(nBytes, payload);
        return (true);
    }
    else return (false);
}


bool CANController::hasPacket(uint32_t id) {
    return (packetList.find(id) != packetList.end());
}

























/*

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  J1939_SPN
J1939_SPN::J1939_SPN(uint16_t spn) {
    this->spn = spn;
}

void J1939_SPN::parse(uint8_t dataPayload[], uint8_t numBytes) {}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  J1939_PGN
J1939_PGN::J1939_PGN(uint32_t pgn) {
    this->pgn = pgn;
    this->lastReceived = 0;
}

bool J1939_PGN::addSPN(J1939_SPN * spnPtr) {
    int spn = spnPtr->spn;
    if (!hasSPN(spn)) {
        SPNList.insert({spn, spnPtr});
        return true;
    }
    else {
        return false;
    }
}

bool J1939_PGN::hasSPN(uint16_t spn) {
    return (SPNList.find(spn) != SPNList.end());
}

bool J1939_PGN::isValid() {
    return (unsigned long)((millis() - this->lastReceived) >= CAN_INVALID_TIMEOUT); 
}

void J1939_PGN::parse(uint8_t dataPayload[], uint8_t numBytes) {
    // Parse via SPN's first

    // Last we update lastReceived to calculate "valid" condition
    this->lastReceived = millis(); // 
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  J1939_Controller
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
J1939_Controller::J1939_Controller() {}

bool J1939_Controller::hasPGN(uint32_t pgn) {
    return (PGNList.find(pgn) != PGNList.end());
}

bool J1939_Controller::addPGN(J1939_PGN * pgnPtr) {
    int pgn = pgnPtr->pgn;
    if (!hasPGN(pgn)) {
        PGNList.insert({pgn, pgnPtr});
        return true;
    }
    else return false;
}

bool J1939_Controller::parse() {
    return false;
}

*/