#pragma once

#include "Particle.h"

#include <unordered_map>

#define CAN_INVALID_TIMEOUT 5000

class CANParameter {
    public: 
        CANParameter(uint16_t id);

        void parse(uint8_t nBytes, uint8_t payload[64]);
        float getVal();

    public:
        uint16_t id;
        uint8_t bitLen, posByte, posBit;
        float resolution, offset, minVal, maxVal;

    private:
        float paramVal;
};

class CANPacket {
    public: 
        CANPacket(uint32_t id);

        bool addParameter(CANParameter *paramPtr);
        bool getParamVal(uint16_t paramId, float &retVal);
        void parse(uint8_t nBytes, uint8_t payload[64]);
    
    public: 
        uint32_t id;

    private:
        bool isValid();
        bool hasParam(uint16_t paramId);

    private:
        std::unordered_map<uint16_t, CANParameter*> paramList;  
        unsigned long lastRec;
        unsigned long timeout_ms;
};

class CANController {
    public:
        CANController();

        bool addPacket(CANPacket *packetPtr); // add a complete packet object to packetList in setup()
        bool getParamVal(uint16_t paramId);
        bool parse(uint32_t id, uint8_t nBytes, uint8_t payload[64]); 
    
    private:
        bool hasPacket(uint32_t packetId);

    private:
        std::unordered_map<uint32_t, CANPacket*> packetList;  
};
