#pragma once

/* TCAN4550 library by Alex J Barry
 */

// This will load the definition for common Particle variable types
#include "Particle.h"
#include "TCAN4x5x_Data_Structs.h"
#include "TCAN4x5x_Reg.h"

#define TCAN4550_SELECT()   digitalWrite(TCANCS, LOW)
#define TCAN4550_UNSELECT() digitalWrite(TCANCS, HIGH)


//! If TCAN4x5x_MCAN_CACHE_CONFIGURATION is defined, then the read and write to MRAM functions will cache certain values to reduce the number of SPI reads necessary to send or receive a packet
#define TCAN4x5x_MCAN_CACHE_CONFIGURATION

//! If TCAN4x5x_MCAN_VERIFY_CONFIGURATION_WRITES is defined, then each MCAN configuration write will be read and verified for correctness
#define TCAN4x5x_MCAN_VERIFY_CONFIGURATION_WRITES

//! If TCAN4x5x_DEVICE_VERIFY_CONFIGURATION_WRITES is defined, then each device configuration write will be read and verified for correctness
#define TCAN4x5x_DEVICE_VERIFY_CONFIGURATION_WRITES


// Defines for caching the configuration values to save on reads and writes
#ifdef TCAN4x5x_MCAN_CACHE_CONFIGURATION
#define TCAN4x5x_MCAN_CACHE_SIDFC           0
#define TCAN4x5x_MCAN_CACHE_XIDFC           1
#define TCAN4x5x_MCAN_CACHE_RXF0C           2
#define TCAN4x5x_MCAN_CACHE_RXF1C           3
#define TCAN4x5x_MCAN_CACHE_RXBC            4
#define TCAN4x5x_MCAN_CACHE_TXEFC           5
#define TCAN4x5x_MCAN_CACHE_TXBC            6
#define TCAN4x5x_MCAN_CACHE_RXESC           7
#define TCAN4x5x_MCAN_CACHE_TXESC           8
#endif

//------------------------------------------------------------------------
// AHB Access Op Codes
//------------------------------------------------------------------------
#define AHB_WRITE_OPCODE                            0x61
#define AHB_READ_OPCODE                             0x41

typedef enum { RXFIFO0, RXFIFO1 } TCAN4x5x_MCAN_FIFO_Enum;
typedef enum { TCAN4x5x_WDT_60MS, TCAN4x5x_WDT_600MS, TCAN4x5x_WDT_3S, TCAN4x5x_WDT_6S } TCAN4x5x_WDT_Timer_Enum;
typedef enum { TCAN4x5x_DEVICE_TEST_MODE_NORMAL, TCAN4x5x_DEVICE_TEST_MODE_PHY, TCAN4x5x_DEVICE_TEST_MODE_CONTROLLER } TCAN4x5x_Device_Test_Mode_Enum;
typedef enum { TCAN4x5x_DEVICE_MODE_NORMAL, TCAN4x5x_DEVICE_MODE_STANDBY, TCAN4x5x_DEVICE_MODE_SLEEP } TCAN4x5x_Device_Mode_Enum;


// This is your main class that users will import into their application
class TCAN4550 {

  public:
    TCAN4550(uint8_t _CS);
    
  //PRIVATE Functions
  public:
    // ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    //                            MCAN Device Functions
    // ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    bool TCAN4x5x_MCAN_EnableProtectedRegisters(void);
    bool TCAN4x5x_MCAN_DisableProtectedRegisters(void);
    bool TCAN4x5x_MCAN_ConfigureCCCRRegister(TCAN4x5x_MCAN_CCCR_Config *cccr);
    void TCAN4x5x_MCAN_ReadCCCRRegister(TCAN4x5x_MCAN_CCCR_Config *cccrConfig);
    void TCAN4x5x_MCAN_ReadDataTimingFD_Simple(TCAN4x5x_MCAN_Data_Timing_Simple *dataTiming);
    void TCAN4x5x_MCAN_ReadDataTimingFD_Raw(TCAN4x5x_MCAN_Data_Timing_Raw *dataTiming);
    bool TCAN4x5x_MCAN_ConfigureDataTiming_Simple(TCAN4x5x_MCAN_Data_Timing_Simple *dataTiming);
    bool TCAN4x5x_MCAN_ConfigureDataTiming_Raw(TCAN4x5x_MCAN_Data_Timing_Raw *dataTiming);
    void TCAN4x5x_MCAN_ReadNominalTiming_Simple(TCAN4x5x_MCAN_Nominal_Timing_Simple *nomTiming);
    void TCAN4x5x_MCAN_ReadNominalTiming_Raw(TCAN4x5x_MCAN_Nominal_Timing_Raw *nomTiming);
    bool TCAN4x5x_MCAN_ConfigureNominalTiming_Simple(TCAN4x5x_MCAN_Nominal_Timing_Simple *nomTiming);
    bool TCAN4x5x_MCAN_ConfigureNominalTiming_Raw(TCAN4x5x_MCAN_Nominal_Timing_Raw *nomTiming);
    bool TCAN4x5x_MCAN_ConfigureGlobalFilter(TCAN4x5x_MCAN_Global_Filter_Configuration *gfc);

    bool TCAN4x5x_MRAM_Configure(TCAN4x5x_MRAM_Config *MRAMConfig);
    void TCAN4x5x_MRAM_Clear(void);
    bool TCAN4x5x_MCAN_ReadXIDFilter(uint8_t filterIndex, TCAN4x5x_MCAN_XID_Filter *filter);
    void TCAN4x5x_MCAN_ReadInterrupts(TCAN4x5x_MCAN_Interrupts *ir);
    void TCAN4x5x_MCAN_ClearInterrupts(TCAN4x5x_MCAN_Interrupts *ir);
    void TCAN4x5x_MCAN_ClearInterruptsAll(void);
    void TCAN4x5x_MCAN_ReadInterruptEnable(TCAN4x5x_MCAN_Interrupt_Enable *ie);
    void TCAN4x5x_MCAN_ConfigureInterruptEnable(TCAN4x5x_MCAN_Interrupt_Enable *ie);
    uint8_t TCAN4x5x_MCAN_ReadNextFIFO(TCAN4x5x_MCAN_FIFO_Enum FIFODefine, TCAN4x5x_MCAN_RX_Header *header, uint8_t dataPayload[]);
    uint8_t TCAN4x5x_MCAN_ReadRXBuffer(uint8_t bufIndex, TCAN4x5x_MCAN_RX_Header *header, uint8_t dataPayload[]);
    uint32_t TCAN4x5x_MCAN_WriteTXBuffer(uint8_t bufIndex, TCAN4x5x_MCAN_TX_Header *header, uint8_t dataPayload[]);
    bool TCAN4x5x_MCAN_TransmitBufferContents(uint8_t bufIndex);
    bool TCAN4x5x_MCAN_WriteSIDFilter(uint8_t filterIndex, TCAN4x5x_MCAN_SID_Filter *filter);
    bool TCAN4x5x_MCAN_ReadSIDFilter(uint8_t filterIndex, TCAN4x5x_MCAN_SID_Filter *filter);
    bool TCAN4x5x_MCAN_WriteXIDFilter(uint8_t fifoIndex, TCAN4x5x_MCAN_XID_Filter *filter);
    uint8_t TCAN4x5x_MCAN_DLCtoBytes(uint8_t inputDLC);
    uint8_t TCAN4x5x_MCAN_TXRXESC_DataByteValue(uint8_t inputESCValue);

    // ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    //                            Non-MCAN Device Functions
    // ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
    uint16_t TCAN4x5x_Device_ReadDeviceVersion(void);
    bool TCAN4x5x_Device_Configure(TCAN4x5x_DEV_CONFIG *devCfg);
    void TCAN4x5x_Device_ReadConfig(TCAN4x5x_DEV_CONFIG *devCfg);
    void TCAN4x5x_Device_ReadInterrupts(TCAN4x5x_Device_Interrupts *ir);
    void TCAN4x5x_Device_ClearInterrupts(TCAN4x5x_Device_Interrupts *ir);
    void TCAN4x5x_Device_ClearInterruptsAll(void);
    void TCAN4x5x_Device_ClearSPIERR(void);
    void TCAN4x5x_Device_ReadInterruptEnable(TCAN4x5x_Device_Interrupt_Enable *ie);
    bool TCAN4x5x_Device_ConfigureInterruptEnable(TCAN4x5x_Device_Interrupt_Enable *ie);
    bool TCAN4x5x_Device_SetMode(TCAN4x5x_Device_Mode_Enum modeDefine);
    TCAN4x5x_Device_Mode_Enum TCAN4x5x_Device_ReadMode(void);
    bool TCAN4x5x_Device_EnableTestMode(TCAN4x5x_Device_Test_Mode_Enum modeDefine);
    bool TCAN4x5x_Device_DisableTestMode(void);
    TCAN4x5x_Device_Test_Mode_Enum TCAN4x5x_Device_ReadTestMode(void);

    bool TCAN4x5x_WDT_Configure(TCAN4x5x_WDT_Timer_Enum WDTtimeout);
    TCAN4x5x_WDT_Timer_Enum TCAN4x5x_WDT_Read(void);
    bool TCAN4x5x_WDT_Enable(void);
    bool TCAN4x5x_WDT_Disable(void);
    void TCAN4x5x_WDT_Reset(void);

  // SPI ABSTRACTION
  private:
    //------------------------------------------------------------------------
    //							Write Functions
    //------------------------------------------------------------------------
    void AHB_WRITE_32(uint16_t address, uint32_t data);
    void AHB_WRITE_BURST_START(uint16_t address, uint8_t words);
    void AHB_WRITE_BURST_WRITE(uint32_t data);
    void AHB_WRITE_BURST_END(void);

    //--------------------------------------------------------------------------
    //							Read Functions
    //--------------------------------------------------------------------------
    uint32_t AHB_READ_32(uint16_t address);
    void AHB_READ_BURST_START(uint16_t address, uint8_t words);
    uint32_t AHB_READ_BURST_READ(void);
    void AHB_READ_BURST_END(void);

  private:
    uint8_t TCANCS; //chip select pin for SPI
};
