/******************************************************/
//       THIS IS A GENERATED FILE - DO NOT EDIT       //
/******************************************************/

#line 1 "d:/Jdex/jdex_bb8/jdex_bb8_firmware/src/jdex_bb8_firmware.ino"
/*
 * PROJECT:     jdex_bb8_firmware
 * DECRIPTION:  Firmware to run on jdex_bb8 hardware, based on Particle bsom
 * AUTHOR:      alex@ajbarry.com.au
 * DATE:        2020/04
 */

// DEFINE THIS TO DISABLE CELLULAR CODE FOR TESTING IN AUS
//#define ETHERNET_ONLY_BSOM
#define CLOUD_IS_DISABLED

#include "Particle.h"
void setup();
void loop();
void getTCAN();
void tx();
void sendTCAN();
void getCellAntenna();
#line 13 "d:/Jdex/jdex_bb8/jdex_bb8_firmware/src/jdex_bb8_firmware.ino"
#include "TCAN4550.h" 

SYSTEM_THREAD(ENABLED);
SYSTEM_MODE(MANUAL);

// IO PIN ALLOCATIONS
#define PIN_CAN0_INT D5     // _INT Pin for TCAN
#define PIN_CAN0_RST D6     // RST PIN for TCAN
#define PIN_SPI_CS D8   // SPI CHIP SELECT FOR TCAN
#define PIN_RES_IN A1     // ADC1 
#define PIN_VBAT_1 A2     // ADC2 
#define PIN_VBAT_2 A3     // ADC3 
#define PIN_VBAT_3 A4     // ADC4 
#define PIN_AN_1 A5       // ADC5 (*RESERVED)
#define PIN_AN_2 A6       // ADC6 (*RESERVED)
#define PIN_HS_OUT_1 D22  // GPIO0 
#define PIN_HS_OUT_2 D23  // GPIO1 
#define PIN_PWROFF D4     // PWM0 
#define PIN_TCAN_RST D6   // PWM2

// DEFINE HARDCODED MAGIC NUMBERS
#define SERIAL_PRINT_RATE 5000 // milliseconds

// DEFINE CAN ID'S
#define CAN_ID_CONTROL 0x10EF0078

// INSTANTIATE TCAN4550 OBJECT WITH SPI CS PIN
TCAN4550 CAN0(PIN_SPI_CS);

// GLOBAL VARIABLES
volatile uint8_t TCAN_Int_Cnt = 0;  // A variable used to keep track of interrupts the MCAN Interrupt pin
unsigned long g_lastAnalogCheck = 0;
unsigned int g_HS_Output_1 = 0;
unsigned int g_HS_Output_2 = 0;

// FUNCTION PROTOTYPES
void intCounter();
void init_SPI();
void init_TCAN();
void getCloudStatus();
void getCellInfo();
void getCellICCID();
bool serialRateElapsed();
void getAnalogs();
void setHSOutput(uint output, uint state);
void toggleHSOutputs();
int callbackICCID(int type, const char* buf, int len, char* iccid);
int callbackAntenna(int type, const char* buf, int len, char* antenna);
void handleCANInterrupt();

void powerCtrl_StrobeDone();

void tcan_printInterrupts();

#ifdef ETHERNET_ONLY_BSOM
// PRE SETUP FUNCTIONS
STARTUP(System.enableFeature(FEATURE_ETHERNET_DETECTION));
#endif // ETHERNET_ONLY_BSOM

// SETUP()
void setup() {
    Serial.begin();
    delay(3000);

    Serial.println("setup()");
    
    init_SPI();
    delay(500);
    
    init_TCAN(); 

    pinMode(PIN_HS_OUT_1, OUTPUT);
    pinMode(PIN_HS_OUT_2, OUTPUT);
    pinMode(PIN_TCAN_RST, OUTPUT);
    digitalWrite(PIN_TCAN_RST, LOW);
    pinMode(PIN_PWROFF, OUTPUT);
    digitalWrite(PIN_PWROFF, LOW);

#ifndef ETHERNET_ONLY_BSOM
    Cellular.on();
    delay(1000);
    Cellular.connect();

    getCellICCID();   // Note will block for up to 10s
    getCellAntenna(); // Note will block for up to 10s
#endif // ETHERNET_ONLY_BSOM

}

// LOOP()
void loop() {

    Particle.process();

    // CAN BUS
    if (TCAN_Int_Cnt > 0) {
      handleCANInterrupt();              
    }

    if (serialRateElapsed()) {  
      // PARTICLE CONNECTION STATUS
      getCloudStatus();

      // CELL SIGNAL
      getCellInfo();

      // ANALOG INPUTS
      getAnalogs();

      // DIGITAL OUTPUTS
      Serial.printlnf("PIN_HS_OUT_1: %d", g_HS_Output_1);
      Serial.printlnf("PIN_HS_OUT_2: %d", g_HS_Output_2);
      //toggleHSOutputs();

      // INTERRUPT COUNT
      Serial.println("\n// INTERRUPT COUNT //");
      Serial.printlnf("TCAN_Int_Cnt:  %d", TCAN_Int_Cnt);

      // TCAN STATUS
      getTCAN();
      sendTCAN();
      tcan_printInterrupts();

      // GET TCAN MODE
      TCAN4x5x_Device_Mode_Enum mode = CAN0.TCAN4x5x_Device_ReadMode();
      Serial.printlnf("Current mode: %d", mode);
      if (mode == TCAN4x5x_DEVICE_MODE_SLEEP) {
        Serial.println("Sleeping, attempting manual wakeup");
        digitalWrite(PIN_TCAN_RST, HIGH);
        delayMicroseconds(1);
        digitalWrite(PIN_TCAN_RST, LOW);
        delayMicroseconds(1000);
      }
      else
      if (mode == TCAN4x5x_DEVICE_MODE_STANDBY) {
        //CAN0.TCAN4x5x_Device_SetMode(TCAN4x5x_DEVICE_MODE_NORMAL); // Standby suggest CAN WUP event, so go to normal mode 
        init_TCAN(); 
      }
    }
}

void handleCANInterrupt() {
  Serial.println("\n// CAN RX //");

  TCAN_Int_Cnt--;
  TCAN4x5x_Device_Interrupts dev_ir = {0};        // Define a new Device IR object for device (non-CAN) interrupt checking
  TCAN4x5x_MCAN_Interrupts mcan_ir = {0};			    // Setup a new MCAN IR object for easy interrupt checking
  CAN0.TCAN4x5x_Device_ReadInterrupts(&dev_ir);   // Read the device interrupt register
  CAN0.TCAN4x5x_MCAN_ReadInterrupts(&mcan_ir);		// Read the interrupt register

  if (dev_ir.SPIERR) {                                 // If the SPIERR flag is set
      CAN0.TCAN4x5x_Device_ClearSPIERR();              // Clear the SPIERR flag
      Log.info("SPIERR flag was set, clearing");
  }

  if (mcan_ir.RF0N)	{								              // If a new message in RX FIFO 0
    TCAN4x5x_MCAN_RX_Header MsgHeader = {0};		  // Initialize to 0 or you'll get garbage
    uint8_t numBytes = 0;                         // Used since the ReadNextFIFO function will return how many bytes of data were read
    uint8_t dataPayload[64] = {0};                // Used to store the received data

    CAN0.TCAN4x5x_MCAN_ClearInterrupts(&mcan_ir);	    // Clear any of the interrupt bits that are set.

    numBytes = CAN0.TCAN4x5x_MCAN_ReadNextFIFO( RXFIFO0, &MsgHeader, dataPayload);	// This will read the next element in the RX FIFO 0
    
    if (MsgHeader.XTD == 1) {
      Serial.printlnf("Rx Message id: 0x%08X", MsgHeader.ID);
      if (MsgHeader.ID == CAN_ID_CONTROL) {
        uint8_t cmd = dataPayload[0];
        if (cmd == 0x10) {
          uint8_t DO1 = (dataPayload[1] & 0x03); //bits 0-1: D01 command
          uint8_t DO2 = (dataPayload[1] & 0x0C) >> 2; // bits 2-3: DO2 command
          Serial.printlnf("WRITE_IO_0: %d, %d", DO1, DO2);

          setHSOutput(PIN_HS_OUT_1 , DO1);
          setHSOutput(PIN_HS_OUT_2 , DO2);

          uint8_t goToStby = (dataPayload[2] & 0xFF);
          if (goToStby == 0x69) {
            Serial.println("\nGoing to Stby");
            CAN0.TCAN4x5x_Device_SetMode(TCAN4x5x_DEVICE_MODE_SLEEP);
          }
        }
      }
    }
    else {
      Serial.printlnf("Rx Message id: 0x%03X", MsgHeader.ID);
    }

    Serial.print("[");
    for(int i=0; i< numBytes; i++) {
      Serial.printf("%02X ", dataPayload[i]);
    }

    Serial.printlnf("]"); 
  }
}

bool serialRateElapsed() {
  unsigned long now = millis();
  bool elapsed = ((unsigned long)(now - g_lastAnalogCheck) >= SERIAL_PRINT_RATE);
  if(elapsed) g_lastAnalogCheck = now;
  return elapsed;
}

void intCounter() {
    TCAN_Int_Cnt++;
}

void getTCAN() {
    Serial.println("\n// TCAN4x5x //");
    uint16_t version = 0x0000;
    version = CAN0.TCAN4x5x_Device_ReadDeviceVersion();
    Serial.printlnf("Version: 0x%02X", version);
}

class CANOutPacket {
  public:
    uint8_t payload[8] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
    TCAN4x5x_MCAN_TX_Header header = {0};

    CANOutPacket(uint8_t cmd) {
      payload[0] = cmd;
      
      header.DLC = MCAN_DLC_8B;						    // Set the DLC to be equal to or less than the data payload (it is ok to pass a 64 byte data array into the WriteTXFIFO function if your DLC is 8 bytes, only the first 8 bytes will be read)
      header.ID  = 0x18EF1100;								// Set the ID to: PGN 61184, PDU_S 0x11
      header.FDF = 0;									        // CAN FD frame disabled
      header.BRS = 0;									// Bit rate switch disabled
      header.EFC = 0;
      header.MM  = 0;
      header.RTR = 0;
      header.XTD = 1;									// Extended ID
      header.ESI = 0;									// Error state indicator
    }

    void tx() {
      CAN0.TCAN4x5x_MCAN_WriteTXBuffer(0, &header, payload);
	    CAN0.TCAN4x5x_MCAN_TransmitBufferContents(0);  // might need to queue this instead of immediate tx
    }
};

void tcan_printInterrupts() {
  TCAN4x5x_Device_Interrupts dev_ir = {0};        // Define a new Device IR object for device (non-CAN) interrupt checking
  TCAN4x5x_MCAN_Interrupts mcan_ir = {0};			    // Setup a new MCAN IR object for easy interrupt checking
  CAN0.TCAN4x5x_Device_ReadInterrupts(&dev_ir);   // Read the device interrupt register
  CAN0.TCAN4x5x_MCAN_ReadInterrupts(&mcan_ir);		// Read the interrupt register

  Serial.println("\n // TCAN INTERRUPTS //");
  Serial.printlnf("dev_ir: %X", dev_ir.word);
  Serial.printlnf("mcan_ir: %X", mcan_ir.word);
}

#define CMD_READ_INPUTS_0   0x01
#define CMD_READ_OUTPUTS_0  0x02
#define READ_CELL_0         0x03

void sendTCAN() {
    // OutPacket read_inputs_0(CMD_READ_INPUTS_0);

    CANOutPacket read_outputs_0(CMD_READ_OUTPUTS_0);
    read_outputs_0.payload[1] = g_HS_Output_1 + (g_HS_Output_2 << 2) + (0xFFFFFF << 4);
    read_outputs_0.tx();
}

void getCloudStatus() {
      Serial.println("\n// PARTICLE CONNECTION STATUS //");
      if(Particle.connected() == false) {
        Serial.println("Cloud Offline");//, Connecting..");
        
#ifndef CLOUD_IS_DISABLED
        Particle.connect();
        waitFor(Particle.connected, 60000);
#endif //CLOUD_IS_DISABLED
      }
      else Serial.println("Cloud Connected");
}

void getCellInfo() {
    Serial.println("\n// CELL SIGNAL //");
    CellularSignal sig = Cellular.RSSI(); 
    int rat = sig.getAccessTechnology();
    switch (rat) {
      case NET_ACCESS_TECHNOLOGY_GSM:
        Serial.println("CELL: 2G RAT");
        break;
      case NET_ACCESS_TECHNOLOGY_EDGE:
        Serial.println("CELL: 2G RAT with EDGE");
        break;
      case NET_ACCESS_TECHNOLOGY_UMTS:
        Serial.println("CELL: UMTS RAT");
        break;
      case NET_ACCESS_TECHNOLOGY_LTE:
        Serial.println("CELL: LTE RAT");
        break;
      default:
        Serial.println("CELL: Unknown RAT");
        break;
    }

    float sigQual = sig.getQuality();
    float sigStr  = sig.getStrength();

    // DEBUG
    //rat = 1;
    //sigStr = 82.4;
    //sigQual = 77.8;

    Serial.printlnf("CELL: Signal QUAL: %.02f%%, STR: %.02f%%", sigQual, sigStr);

    CANOutPacket read_cell_0(READ_CELL_0);
    // Byte 1 = RAT Type
    read_cell_0.payload[1] = (uint8_t)(rat & 0xFF); 
    // Byte 2/3 = Signal Strength (0.01% / bit)
    read_cell_0.payload[2] = (uint8_t)((uint)(sigStr/0.01) & 0xFF);
    read_cell_0.payload[3] = (uint8_t)(((uint)(sigStr/0.01) >> 8 ) & 0xFF); 
    // Byte 4/5 = Signal Quality (0.01% / bit)
    read_cell_0.payload[4] = (uint8_t)((uint)((sigQual/0.01)) & 0xFF);
    read_cell_0.payload[5] = (uint8_t)(((uint)(sigQual/0.01) >> 8 ) & 0xFF);

    Serial.printlnf("[0x%02X,0x%02X,0x%02X,0x%02X,0x%02X,0x%02X,0x%02X,0x%02X]", read_cell_0.payload[0], read_cell_0.payload[1], read_cell_0.payload[2], read_cell_0.payload[3], read_cell_0.payload[4], read_cell_0.payload[5], read_cell_0.payload[6], read_cell_0.payload[7]);

    read_cell_0.tx();
}

void getCellICCID() {
  Serial.println("\n// CELL ICCID //");
  char iccid[32] = "";
  if ((RESP_OK == Cellular.command(callbackICCID, iccid, 10000, "AT+CCID\r\n"))
    && (strcmp(iccid,"") != 0))
  {
    Serial.printlnf("SIM ICCID = %s\r\n", iccid);
  }
  else
  {
    Serial.println("SIM ICCID NOT FOUND!");
  }
}

void getCellAntenna() {
  Serial.println("\n// CELL ANTENNA //");
  char antenna[32] = "";
  // int antenna = -1 
  
  if ((RESP_OK == Cellular.command(callbackAntenna, antenna, 10000, "AT+UANTR=0\r\n")) && (strcmp(antenna,"") != 0)) {
    Serial.printlnf("Antenna = %s\r\n", antenna);
  }
  else {
    Serial.println("BAD ANTENNA RESPONSE!");
  }
}

// Get the ICCID number of the inserted SIM card
int callbackICCID(int type, const char* buf, int len, char* iccid)
{
  if ((type == TYPE_PLUS) && iccid) {
    if (sscanf(buf, "\r\n+CCID: %[^\r]\r\n", iccid) == 1) {
      /*nothing*/;
    }
  }
  return WAIT;
}

// Get the DC antenna load with resolution of 1kOhm
//  -1 = open circuit, 0 = short circuit, 1-52 = kOhm load
int callbackAntenna(int type, const char* buf, int len, char* antenna) {
  if ((type == TYPE_PLUS) && antenna) {
    if (sscanf(buf, "\r\n+UANTR: %[^\r]\r\n", antenna) == 1) {
      /*nothing*/;
    }
  }
  return WAIT;
}

void getAnalogs() {
      Serial.println("\n// ANALOG INPUT //");

      int32_t res = analogRead(PIN_RES_IN);  
      Serial.printlnf("PIN_RES_IN: %d / 4095", res);

      int32_t vbat1 = analogRead(PIN_VBAT_1);  
      Serial.printlnf("PIN_VBAT_1: %d / 4095", vbat1);

      int32_t vbat2 = analogRead(PIN_VBAT_2);  
      Serial.printlnf("PIN_VBAT_2: %d / 4095", vbat2);

      int32_t vbat3 = analogRead(PIN_VBAT_3);  
      Serial.printlnf("PIN_VBAT_3: %d / 4095", vbat3);

      int32_t an1 = analogRead(PIN_AN_1);  
      Serial.printlnf("PIN_AN_1: %d / 4095", an1);

      int32_t an2 = analogRead(PIN_AN_2);  
      Serial.printlnf("PIN_AN_2: %d / 4095", an2);
}

void toggleHSOutputs() {
  Serial.println("\n// DIGITAL OUTPUTS //");

  if (g_HS_Output_1 == 0) {
    digitalWrite(PIN_HS_OUT_1, HIGH);
    g_HS_Output_1 = 1;
  }
  else {
    digitalWrite(PIN_HS_OUT_1, LOW);
    g_HS_Output_1 = 0;
  }

  if (g_HS_Output_2 == 0) {
    digitalWrite(PIN_HS_OUT_2, HIGH);
    g_HS_Output_2 = 1;
  }
  else {
    digitalWrite(PIN_HS_OUT_2, LOW);
    g_HS_Output_2 = 0;
  }

  Serial.printlnf("PIN_HS_OUT_1: %d", g_HS_Output_1);
  Serial.printlnf("PIN_HS_OUT_2: %d", g_HS_Output_2);

}

// CONFIGURE SPI0
void init_SPI() {
    SPI.setClockSpeed(4, MHZ);
    SPI.setDataMode(SPI_MODE0);
    SPI.begin(SPI_MODE_MASTER, PIN_SPI_CS);
}

// CONFIGURE TCAN4550 AND RELEVANT PIN OUTS
void init_TCAN() {
    pinMode(PIN_CAN0_INT, INPUT_PULLUP); // Configuring pin for /INT input
    attachInterrupt(PIN_CAN0_INT, intCounter, FALLING); //look for high to low changes on interrupt pin

    pinMode(PIN_CAN0_RST, OUTPUT); // Pull RST pin low when not used
    digitalWrite(PIN_CAN0_RST, LOW);

    CAN0.TCAN4x5x_Device_ClearSPIERR();                              // Clear any SPI ERR flags that might be set as a result of our pin mux changing during MCU startup

    // Step one attempt to clear all interrupts
    TCAN4x5x_Device_Interrupt_Enable dev_ie = {0};				// Initialize to 0 to all bits are set to 0.
    CAN0.TCAN4x5x_Device_ConfigureInterruptEnable(&dev_ie);	        // Disable all non-MCAN related interrupts for simplicity

    TCAN4x5x_Device_Interrupts dev_ir = {0};					// Setup a new MCAN IR object for easy interrupt checking
    CAN0.TCAN4x5x_Device_ReadInterrupts(&dev_ir);					// Request that the struct be updated with current DEVICE (not MCAN) interrupt values

    if (dev_ir.PWRON)                                           // If the Power On interrupt flag is set
      CAN0.TCAN4x5x_Device_ClearInterrupts(&dev_ir);               // Clear it because if it's not cleared within ~4 minutes, it goes to sleep

    // Configure the CAN bus speeds
    TCAN4x5x_MCAN_Nominal_Timing_Simple TCANNomTiming = {0};	// 250k arbitration with a 20 MHz crystal ((20E6 / 2) / (32 + 8) = 250E3)
    TCANNomTiming.NominalBitRatePrescaler = 2;
    TCANNomTiming.NominalTqBeforeSamplePoint = 32;
    TCANNomTiming.NominalTqAfterSamplePoint = 8;

    TCAN4x5x_MCAN_Data_Timing_Simple TCANDataTiming = {0};   // 1 Mbps baud rate with a 20 Mhz crystal  ((20E6 / 1) / (15 + 5) = 100E4)
    TCANDataTiming.DataBitRatePrescaler = 1;
    TCANDataTiming.DataTqBeforeSamplePoint = 15;
    TCANDataTiming.DataTqAfterSamplePoint = 5;

    // Configure the MCAN core settings
    TCAN4x5x_MCAN_CCCR_Config cccrConfig = {0};					// Remember to initialize to 0, or you'll get random garbage!
    cccrConfig.FDOE = 0;										// TCAN4x5x_GFC_REJECT disable
    cccrConfig.BRSE = 0;										// CAN FD Bit rate switch disable

    // Configure the default CAN packet filtering settings
    TCAN4x5x_MCAN_Global_Filter_Configuration gfc = {0};
    gfc.RRFE = 1;                                               // Reject remote frames (TCAN4x5x doesn't support this)
    gfc.RRFS = 1;                                               // Reject remote frames (TCAN4x5x doesn't support this)
    gfc.ANFE = TCAN4x5x_GFC_REJECT;                // Default behavior if incoming message doesn't match a filter is to reject all
    gfc.ANFS = TCAN4x5x_GFC_REJECT;                // Default behavior if incoming message doesn't match a filter is to reject all

    // ************************************************************************
    // In the next configuration block, we will set the MCAN core up to have:
    //   - 1 SID filter element
    //   - 1 XID Filter element
    //   - 5 RX FIFO 0 elements
    //   - RX FIFO 0 supports data payloads up to 64 bytes
    //   - RX FIFO 1 and RX Buffer will not have any elements, but we still set their data payload sizes, even though it's not required
    //   - No TX Event FIFOs
    //   - 2 Transmit buffers supporting up to 64 bytes of data payload
    //

    TCAN4x5x_MRAM_Config MRAMConfiguration = {0};
    MRAMConfiguration.SIDNumElements = 1;						            // Standard ID number of elements, you MUST have a filter written to MRAM for each element defined
    MRAMConfiguration.XIDNumElements = 1;						            // Extended ID number of elements, you MUST have a filter written to MRAM for each element defined
    MRAMConfiguration.Rx0NumElements = 5;						            // RX0 Number of elements
    MRAMConfiguration.Rx0ElementSize = MRAM_64_Byte_Data;		    // RX0 data payload size
    MRAMConfiguration.Rx1NumElements = 0;						            // RX1 number of elements
    MRAMConfiguration.Rx1ElementSize = MRAM_64_Byte_Data;		    // RX1 data payload size
    MRAMConfiguration.RxBufNumElements = 0;						          // RX buffer number of elements
    MRAMConfiguration.RxBufElementSize = MRAM_64_Byte_Data;	    // RX buffer data payload size
    MRAMConfiguration.TxEventFIFONumElements = 0;				        // TX Event FIFO number of elements
    MRAMConfiguration.TxBufferNumElements = 2;					        // TX buffer number of elements
    MRAMConfiguration.TxBufferElementSize = MRAM_8_Byte_Data;	  // TX buffer data payload size

    // Configure the MCAN core with the settings above, the changes in this block are write protected registers,    
    // so it makes the most sense to do them all at once, so we only unlock and lock once                        

    CAN0.TCAN4x5x_MCAN_EnableProtectedRegisters();					          // Start by making protected registers accessible
    CAN0.TCAN4x5x_MCAN_ConfigureCCCRRegister(&cccrConfig);			      // Disable FD mode and Bit rate switching
    CAN0.TCAN4x5x_MCAN_ConfigureGlobalFilter(&gfc);                   // Configure the global filter configuration (Default CAN message behavior)
    CAN0.TCAN4x5x_MCAN_ConfigureNominalTiming_Simple(&TCANNomTiming); // Setup nominal/arbitration bit timing
    CAN0.TCAN4x5x_MCAN_ConfigureDataTiming_Simple(&TCANDataTiming);	  // Setup CAN FD timing
    CAN0.TCAN4x5x_MRAM_Clear();										                    // Clear all of MRAM (Writes 0's to all of it)
    CAN0.TCAN4x5x_MRAM_Configure(&MRAMConfiguration);				          // Set up the applicable registers related to MRAM configuration
    CAN0.TCAN4x5x_MCAN_DisableProtectedRegisters();                   // Disable protected write and take device out of INIT mode


    // Set the interrupts we want to enable for MCAN
    TCAN4x5x_MCAN_Interrupt_Enable mcan_ie = {0};				// Remember to initialize to 0, or you'll get random garbage!
    mcan_ie.RF0NE = 1;											            // RX FIFO 0 new message interrupt enable

    CAN0.TCAN4x5x_MCAN_ConfigureInterruptEnable(&mcan_ie);			// Enable the appropriate registers


    // Setup filters, this filter will mark any message with ID 0x055 as a priority message
    TCAN4x5x_MCAN_SID_Filter SID_ID = {0};
    SID_ID.SFT = TCAN4x5x_SID_SFT_CLASSIC;						    // SFT: Standard filter type. Configured as a classic filter
    SID_ID.SFEC = TCAN4x5x_SID_SFEC_PRIORITYSTORERX0;			// Standard filter element configuration, store it in RX fifo 0 as a priority message
    SID_ID.SFID1 = 0x055;										              // SFID1 (Classic mode Filter)
    SID_ID.SFID2 = 0x7FF;										              // SFID2 (Classic mode Mask)
    CAN0.TCAN4x5x_MCAN_WriteSIDFilter(0, &SID_ID);				// Write to the MRAM


    // Store ID 0x10EF0078 as a priority message - USED FOR CAN_BASED_CONTROL
    TCAN4x5x_MCAN_XID_Filter XID_ID = {0};
    XID_ID.EFT = TCAN4x5x_XID_EFT_CLASSIC;                      // EFT
    XID_ID.EFEC = TCAN4x5x_XID_EFEC_PRIORITYSTORERX0;           // EFEC
    XID_ID.EFID1 = CAN_ID_CONTROL;                                  // EFID1 (Classic mode filter)
    XID_ID.EFID2 = 0x1FFFFFFF;                                  // EFID2 (Classic mode mask)
    CAN0.TCAN4x5x_MCAN_WriteXIDFilter(0, &XID_ID);              // Write to the MRAM

    // Configure the TCAN4550 Non-CAN-related functions
    TCAN4x5x_DEV_CONFIG devConfig = {0};                        // Remember to initialize to 0, or you'll get random garbage!
    devConfig.SWE_DIS = 0;                                      // Keep Sleep Wake Error Enabled (it's a disable bit, not an enable)
    devConfig.DEVICE_RESET = 0;                                 // Not requesting a software reset
    devConfig.WD_EN = 0;                                        // Watchdog disabled
    devConfig.nWKRQ_CONFIG = 0;                                 // Mirror INH function (default)
    devConfig.INH_DIS = 0;                                      // INH enabled (default)
    devConfig.GPIO1_GPO_CONFIG = TCAN4x5x_DEV_CONFIG_GPO1_MCAN_INT1;    // MCAN nINT 1 (default)
    devConfig.FAIL_SAFE_EN = 0;                                 // Failsafe disabled (default)
    devConfig.GPIO1_CONFIG = TCAN4x5x_DEV_CONFIG_GPIO1_CONFIG_GPO;      // GPIO set as GPO (Default)
    devConfig.WD_ACTION = TCAN4x5x_DEV_CONFIG_WDT_ACTION_nINT;  // Watchdog set an interrupt (default)
    devConfig.WD_BIT_RESET = 0;                                 // Don't reset the watchdog
    devConfig.nWKRQ_VOLTAGE = 0;                                // Set nWKRQ to internal voltage rail (default)
    devConfig.GPO2_CONFIG = TCAN4x5x_DEV_CONFIG_GPO2_NO_ACTION; // GPO2 has no behavior (default)
    devConfig.CLK_REF = 1;                                      // Input crystal is a 40 MHz crystal (default)
    devConfig.WAKE_CONFIG = TCAN4x5x_DEV_CONFIG_WAKE_BOTH_EDGES;// Wake pin can be triggered by either edge (default)
    CAN0.TCAN4x5x_Device_Configure(&devConfig);                      // Configure the device with the above configuration

    CAN0.TCAN4x5x_Device_SetMode(TCAN4x5x_DEVICE_MODE_NORMAL);       // Set to normal mode, since configuration is done. This line turns on the transceiver

    CAN0.TCAN4x5x_MCAN_ClearInterruptsAll();                         // Resets all MCAN interrupts (does NOT include any SPIERR interrupts)
}

// Instruct Timer to disable DRV as we are now done for this wake period
void powerCtrl_StrobeDone() {
    digitalWrite(PIN_PWROFF, HIGH);
    delayMicroseconds(1);
    digitalWrite(PIN_PWROFF, LOW);
}

void setHSOutput(uint output, uint state) {
    if (state != LOW && state != HIGH) return;  // bad state command

    if (output == PIN_HS_OUT_1) g_HS_Output_1 = state;
    else if (output == PIN_HS_OUT_2) g_HS_Output_2 = state;
    else return; // bad PIN command

    digitalWrite(output, state);
}